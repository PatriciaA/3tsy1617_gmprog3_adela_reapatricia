﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class Stats{

	public float VIT;
	public float STR;
	public float currentHP;
	public float currentXP;
	public float maxXP;
	public float lvl;
	public float gold;
	public float currentMana;
	public float maxMana;

	public float maxHP{
		get { return VIT * 10; }
		set { maxHP = value; }
	}

	public float ATK{
		get { return STR * 10; }
		set { ATK = value; }
	}

	public float SkillATK{
		get { return STR * 30; }
		set { SkillATK = value; }
	}
}

public class PlayerStats : MonoBehaviour {

	public Stats playerStats;

	public Text showLevel;
	public Text showHp;
	public Text showATK;
	public Text showSkillATK;
	public Text showGold;
	public Text showExp;

	public Image hp;
	public Image xp;
	public Image mana;


    public GameObject rewardPanel;
	void Start(){
		playerStats.STR = 2;
		playerStats.VIT = 5;
		playerStats.lvl = 1;
		playerStats.currentHP = playerStats.maxHP;
		playerStats.maxXP = 100;
		playerStats.gold = 1000;
		playerStats.maxMana = 50; 
		playerStats.currentMana = playerStats.maxMana;
	}

	void Update(){
		hp.fillAmount = playerStats.currentHP / playerStats.maxHP;
		xp.fillAmount = playerStats.currentXP / playerStats.maxXP;
		mana.fillAmount = playerStats.currentMana / playerStats.maxMana;

		showLevel.text = "Level: " + playerStats.lvl.ToString();
		showHp.text = "HP: " + playerStats.currentHP.ToString ();
		showATK.text = "Attack: " + playerStats.ATK.ToString ();
		showSkillATK.text = "Skill Attack: " + playerStats.SkillATK.ToString ();
		showGold.text = "Gold: " + playerStats.gold.ToString ();
		showExp.text = "EXP: " + playerStats.currentXP.ToString ();

		if (playerStats.currentHP <= 0) {
			playerStats.currentHP = 0;
		}

		if (playerStats.currentHP >= playerStats.maxHP) {
			playerStats.currentHP = playerStats.maxHP;
		}

		if (playerStats.currentXP <= 0) {
			playerStats.currentXP = 0;
		}

		if (playerStats.currentXP >= playerStats.maxXP) {
			playerStats.currentXP = 0;
			LevelUp ();
		}

		if (Input.GetKeyUp (KeyCode.E)) {
			playerStats.currentXP += 10;
		}
	}

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Gold") {
			Destroy (other.gameObject);
			playerStats.gold += 50;

		}

		if (other.gameObject.tag == "item") {
			other.GetComponent<Renderer> ().enabled = false;
			GetComponent<PlayerInventory> ().AddToInventory (other.gameObject);
			//playerStats.currentHP += 20;
		}
	}

	void LevelUp(){
		playerStats.lvl++;
		playerStats.VIT++;
		playerStats.STR++;		
	}

	public void AddExp(float xp){
		playerStats.currentXP += xp;
	}

    public void OnClickRewardButton()
    {
        playerStats.currentXP += 100;
        playerStats.gold += 500;
        rewardPanel.SetActive(false);
    }
}
