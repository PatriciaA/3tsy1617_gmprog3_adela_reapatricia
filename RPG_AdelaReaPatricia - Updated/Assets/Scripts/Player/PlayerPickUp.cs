﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPickUp : MonoBehaviour {

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.tag == "Gold") {
			Destroy (other.gameObject);
			GetComponent<PlayerStats>().playerStats.gold += 50;

		}

		if (other.gameObject.tag == "item") {
			other.GetComponent<Renderer> ().enabled = false;
			GetComponent<PlayerInventory> ().AddToInventory (other.gameObject);
		}
	}
}
