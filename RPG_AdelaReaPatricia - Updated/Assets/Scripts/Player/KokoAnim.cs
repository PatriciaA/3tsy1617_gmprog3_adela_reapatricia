﻿using UnityEngine;
using System.Collections;

public class KokoAnim : MonoBehaviour 
{
	public Animator animatorController;

	public void PlayAnimation(float animation){
		animatorController.SetFloat ("Koko_Animation", animation);
	}
}