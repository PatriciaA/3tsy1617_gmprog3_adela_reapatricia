﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class PlayerInventory : MonoBehaviour {
    public GameObject inventoryPanel;
	public List<GameObject> Inventory = new List<GameObject> ();

	public void AddToInventory (GameObject item)
	{
		Inventory.Add(item);
	}
    public void OnExitBtnClick()
    {
        inventoryPanel.SetActive(false);
    }

}
