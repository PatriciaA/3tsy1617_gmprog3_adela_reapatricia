﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerCombat : MonoBehaviour {

	public GameObject target;
	public float attackrange;
	public float damage;
	Animator anim;

	void Start () {
		anim = GetComponent<Animator> ();
		damage = this.GetComponent<PlayerStats> ().playerStats.ATK;
	}

	void Update () {
		if (Input.GetMouseButtonDown (1)) {
			RaycastHit hit;
			Ray ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.CompareTag ("Monster")) {
					target = hit.collider.gameObject;
				}
			}
		}

	}

	void DamageTo(){
		target.GetComponent<MonsterStats> ().monsterStats.currentHP -= damage;
		Debug.Log ("Attacking Enemy");
	}
}

