﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

    public Vector3 desPoint;
    public UnityEngine.AI.NavMeshAgent agent;
    public Animator animator;
    public GameObject pointer;
    GameObject ptr;

    void Start()
    {
        agent = GetComponent<UnityEngine.AI.NavMeshAgent>();
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            SetTargetPosition();
        }
    }

    void SetTargetPosition()
    {
        Ray ray;
        RaycastHit hit;

        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit))
        {
            desPoint = hit.point;
            CreateCursor();
            animator.SetFloat("Koko_Animation", 1);// this plays the run animation
        }

        if (Vector3.Distance(this.transform.position, desPoint) <= 1.0f)
        {
            Destroy(ptr);
            Debug.Log("working");
            animator.SetFloat("Koko_Animation", 0);
        }

        agent.SetDestination(desPoint);
    }

    void CreateCursor()
    {
        Destroy(ptr);
        ptr = Instantiate(pointer, desPoint + new Vector3(0, 0.5f, 0), Quaternion.identity);
        ptr.transform.SetParent(this.transform.parent);
    }
}
