﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Heal : Koko_Skill {

	public float regenAmount;

	public override void UseSkill ()
	{
		base.UseSkill ();
		this.GetComponent<PlayerStats> ().playerStats.currentHP += regenAmount;
	}
}
