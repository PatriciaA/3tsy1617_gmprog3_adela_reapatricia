﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GroundCrush : Koko_Skill {

	private GameObject eff1, eff2;
	public GameObject specialEffect1, specialEffect2;
	public float range;
	public float dmg;

	public override void UseSkill ()
	{
		base.UseSkill ();
		this.GetComponent<Koko_FSM> ().currState = Koko_FSM.KokoStates.SKILL;
		this.GetComponent<Koko_FSM> ().animator.SetTrigger ("Skill");
	}

	void StartEffect ()
	{
		eff1 = Instantiate (specialEffect1, this.transform.position, Quaternion.identity) as GameObject;
	}

	void AreaAttack ()
	{
		Destroy (eff1);
		eff2 = Instantiate (specialEffect2, this.transform.position, Quaternion.identity) as GameObject;
		Collider[] col = Physics.OverlapSphere (this.transform.position, range);

		foreach (Collider collider in col) {
			if (collider.CompareTag ("Monster"))
			{
				collider.gameObject.GetComponent<MonsterStats> ().monsterStats.currentHP -= dmg;
			}
		}
	}


	void StopEffect()
	{
		Destroy (eff2);
	}
}
