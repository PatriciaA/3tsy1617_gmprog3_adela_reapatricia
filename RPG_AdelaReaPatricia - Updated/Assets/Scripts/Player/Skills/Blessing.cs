﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Blessing : Koko_Skill {

	public bool blessed;
	public float duration;
	public float addVIT, addSTR;
	private float baseDuration;
	private float baseVIT, baseSTR;

	void Update ()
	{
		skillImg.fillAmount = cooldown / baseCooldown;

		if (usedSkill) {
			cooldown -= Time.deltaTime;
			if (blessed) {
				duration -= Time.deltaTime;
				if (duration <= 0) {
					this.GetComponent<PlayerStats> ().playerStats.VIT = baseVIT;
					this.GetComponent<PlayerStats> ().playerStats.STR = baseSTR;
					duration = baseDuration;
					blessed = false;
				}
			}
			if (cooldown <= 0) {
				skillBtn.interactable = true;
				usedSkill = false;
				cooldown = baseCooldown;
			}
		}
	
	}

	public override void UseSkill ()
	{
		base.UseSkill ();
		baseVIT = this.GetComponent<PlayerStats> ().playerStats.VIT;
		baseSTR = this.GetComponent<PlayerStats> ().playerStats.STR;
		baseDuration = duration;
		blessed = true;
		this.GetComponent<PlayerStats> ().playerStats.VIT += addVIT;
		this.GetComponent<PlayerStats> ().playerStats.STR += addSTR;
	}
}
