﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Koko_Skill : MonoBehaviour {

	public bool usedSkill;
	public float manaCost;
	public float cooldown;
	public float baseCooldown;

	public Image skillImg;
	public Button skillBtn;

	void Start()
	{
		cooldown = baseCooldown;
	}

	void Update()
	{
		skillImg.fillAmount = cooldown / baseCooldown;

		if (usedSkill) {
			cooldown -= Time.deltaTime;
			if (cooldown <= 0) {
				usedSkill = false;
				skillBtn.interactable = true;
				cooldown = baseCooldown;
			}
		}
	}

	public virtual void UseSkill()
	{
		if (this.GetComponent<PlayerStats> ().playerStats.currentMana > manaCost) {
			usedSkill = true;
			skillBtn.interactable = false;
			this.GetComponent<PlayerStats> ().playerStats.currentMana -= manaCost;
		}
	}
}
