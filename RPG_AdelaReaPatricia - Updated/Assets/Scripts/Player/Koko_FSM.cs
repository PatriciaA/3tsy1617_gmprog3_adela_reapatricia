﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Koko_FSM : MonoBehaviour {

	public enum KokoStates
	{
		IDLE,
		MOVE,
		CHASE,
		ATTACK,
		SKILL,
		DYING,
		DEAD
	}

	public KokoStates currState;
	public Vector3 desPoint;
	public UnityEngine.AI.NavMeshAgent agent;
	public Animator animator;
	public GameObject pointer;
	public GameObject target;
	GameObject ptr;

	void Start(){
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		animator = GetComponent<Animator> ();
	}

	void Update () {
		switch (currState) {
		case KokoStates.IDLE:
			UpdateIdleState ();
			break;
		case KokoStates.MOVE:
			UpdateMoveState ();
			break;
		case KokoStates.CHASE:
			UpdateChaseState ();
			break;
		case KokoStates.ATTACK:
			UpdateAttackState ();
			break;
		case KokoStates.SKILL:
			UpdateSkillState ();
			break;
		case KokoStates.DYING:
			UpdateDyingState ();
			break;
		case KokoStates.DEAD:
			UpdateDeadState ();
			break;
		}
	}


	protected void UpdateIdleState (){
		animator.SetFloat ("Koko_Animation", 0);
		SetTargetPosition ();
	}

	protected void UpdateMoveState (){
		agent.SetDestination (desPoint);
		SetTargetPosition ();
		if (Vector3.Distance (this.transform.position, desPoint) <= 1.0f) {
			Destroy (ptr);
			currState = KokoStates.IDLE;
		}
	}

	protected void UpdateChaseState (){
		agent.SetDestination (desPoint);
		SetTargetPosition ();
		if (Vector3.Distance (this.transform.position, desPoint) <= 2.0f) {
			animator.SetFloat ("Koko_Animation", 5);
			currState = KokoStates.ATTACK;
		}
	}

	protected void UpdateAttackState (){
		this.transform.LookAt (desPoint);
		desPoint = this.transform.position;
		agent.SetDestination (desPoint);
		SetTargetPosition ();
		if (Vector3.Distance (this.transform.position, desPoint) > 2.0f) {
			currState = KokoStates.CHASE;
		}
	}

	protected void UpdateSkillState()
	{
		if (!this.GetComponent<Koko_Skill> ().usedSkill) {
			SetTargetPosition ();
		}
		else {
			agent.isStopped = true;
		}
	}

	protected void UpdateDyingState (){}
	protected void UpdateDeadState (){}

	void SetTargetPosition() {
		if (Input.GetMouseButtonDown (1)) {

			agent.isStopped = false;
			Ray ray;
			RaycastHit hit;

			ray = Camera.main.ScreenPointToRay (Input.mousePosition);
			if (Physics.Raycast (ray, out hit)) {
				if (hit.collider.CompareTag ("Terrain")) {
					if (target) {
						target.GetComponent<TogglePointer> ().targeted = false;
						target = null;
					}
					desPoint = hit.point;
					CreateCursor ();
					animator.SetFloat ("Koko_Animation", 1);
					currState = KokoStates.MOVE;
				}
				else if (hit.collider.CompareTag ("Monster")) {
					if (target) {
						target.GetComponent<TogglePointer> ().targeted = false;
						target = null;
					}
					Destroy (ptr);
					target = hit.collider.gameObject;
					desPoint = target.transform.position;
					target.GetComponent<TogglePointer> ().targeted = true;
					animator.SetFloat ("Koko_Animation", 2);
					currState = KokoStates.CHASE;
				}
			}
		}
	}

	void CreateCursor(){
		Destroy(ptr);
		ptr = Instantiate (pointer, desPoint + new Vector3(0,0.5f,0), Quaternion.identity);
		ptr.transform.SetParent (this.transform.parent);
	}

	void DamageTo(){
		//target.GetComponent<MonsterStats> ().monsterStats.currentHP -= this.GetComponent<PlayerStats> ().playerStats.ATK;
		if (target == null) {
			return;
		}
		target.GetComponent<MonsterStats>().monsterStats.currentHP = 0;
	}	
}
