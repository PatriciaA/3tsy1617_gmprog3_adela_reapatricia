﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ManaPotion : Item {

	public float manaAmount;
	public Sprite potionSprite;
	public GameObject player;

	public void Use(){
		player.GetComponent<PlayerStats> ().playerStats.currentHP += manaAmount;
	}
}
