﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Store : MonoBehaviour {

	public GameObject[] itemsForSale;
	public GameObject Koko;

	public void PurchasedItem(int itemNum)
	{
		GameObject item = itemsForSale [itemNum];
		Koko.GetComponent<PlayerInventory>().AddToInventory(item);
		Koko.GetComponent<PlayerStats> ().playerStats.gold -= item.GetComponent<Item>().itemCost;
		Debug.Log ("Bought " + item);
	}
}

