﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour {

	public GameObject player;
	public Image xp;

	public float current_xp;
	public float max_xp;
	public float lvl;
	public float current_hp;
	public float max_hp;
	public float atk;
	public float skillAtk;
	public float gold;

	public Text xp_txt;
	public Text lvl_txt;
	public Text hp_txt;
	public Text atk_txt;
	public Text skillAtk_txt;
	public Text gold_txt;

	void Start(){
		
	}

	void Update(){
		current_xp = player.GetComponent<PlayerStats> ().playerStats.currentXP;
		max_xp = player.GetComponent<PlayerStats> ().playerStats.maxXP;
		lvl = player.GetComponent<PlayerStats> ().playerStats.lvl; 
		current_hp = player.GetComponent<PlayerStats> ().playerStats.currentHP;
		max_hp = player.GetComponent<PlayerStats> ().playerStats.maxHP;
		atk = player.GetComponent<PlayerStats> ().playerStats.ATK;
		skillAtk = player.GetComponent<PlayerStats> ().playerStats.SkillATK;
		gold = player.GetComponent<PlayerStats> ().playerStats.gold;

		xp_txt.text = "EXP: " + current_xp.ToString () + " / " + max_xp.ToString ();
		lvl_txt.text = "Level: " + lvl.ToString ();
		hp_txt.text = "HP: " + current_hp.ToString () + " / " + max_hp.ToString ();
		atk_txt.text = "ATK: " + atk.ToString ();
		skillAtk_txt.text = "Skill ATK: " + skillAtk.ToString ();
		gold_txt.text = "Gold: " + gold.ToString ();

		xp.fillAmount = current_xp / max_xp;
	}
}
