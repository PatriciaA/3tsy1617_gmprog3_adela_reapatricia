﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class QuestScript : MonoBehaviour
{
    public Text quest1;
    public Text quest2;
    public GameObject accept1Btn;
    public GameObject accept2Btn;
    public GameObject questComplete;
    public Button submit1Btn;
    public Button submit2Btn;
    public GameObject rewardPanel;
    public int curCount;
  
    void Start()
    {
        
        questComplete.SetActive(false);
    }
    public int AddCount(int count)
    {
        return curCount + count;
    }
    void Update()
    {
        if (curCount >=3)
        {
            submit1Btn.interactable = true;
            questComplete.SetActive(true);
            
        }
        if(curCount>=5)
        {
            submit2Btn.interactable = true;
            questComplete.SetActive(true);
           
        }
        if(Input.GetKey(KeyCode.O))
        {
            accept1Btn.SetActive(true);
            accept2Btn.SetActive(true);
        }
        if(Input.GetKeyDown(KeyCode.P))
        {
            curCount++;
        }
        quest1.text = "Monsters Killed (" + curCount + " / 3)";
//        quest2.text = "Goblins Killed (" + curCount + " / 5)";
    }
    public void OnAccept1BtnClick()
    {
        accept1Btn.SetActive(false);
        submit1Btn.interactable = false;
    }

    public void OnAccept2BtnClick()
    {
        accept2Btn.SetActive(false);
        submit2Btn.interactable = false;
    }
    public void OnSubmitClick()
    {
        curCount = 0;
        questComplete.SetActive(false);
        submit1Btn.interactable = false;
        submit2Btn.interactable = false;
        rewardPanel.SetActive(true);
    }

  
}