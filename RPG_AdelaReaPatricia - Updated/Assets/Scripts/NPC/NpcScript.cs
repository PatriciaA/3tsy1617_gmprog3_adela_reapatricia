﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class NpcScript : MonoBehaviour
{
    public GameObject npcPanel;
    public GameObject shopPanel;
    public GameObject questPanel;
    void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if (Physics.Raycast(ray, out hit, Mathf.Infinity))
            {
                if (hit.collider.tag == "NPC")
                {
                    npcPanel.SetActive(true);
                }
            }
        }
    }

    public void OnExitButtonClick()
    {
        npcPanel.SetActive(false);
        shopPanel.SetActive(false);
        questPanel.SetActive(false);
    }
    public void OnShopBtnClick()
    {
        shopPanel.SetActive(true);
        npcPanel.SetActive(false);
        questPanel.SetActive(false);
    }
    public void OnQuestBtnClick()
    {
        npcPanel.SetActive(false);
        questPanel.SetActive(true);
        shopPanel.SetActive(false);
    }
    
}