﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowStats : MonoBehaviour {

	public GameObject Stats;

	public void OpenStatsBox(){
		Stats.SetActive (true);
	}
}
