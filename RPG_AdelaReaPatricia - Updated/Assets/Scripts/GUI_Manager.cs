﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUI_Manager : MonoBehaviour {

	bool inventory = false;
	public GameObject inventory_UI;

	void Inventory(){
		if (Input.GetKeyDown (KeyCode.I)) {
			if (!inventory) {
				inventory_UI.SetActive (true);
				inventory = true;
			}
			else if (inventory) {
				inventory_UI.SetActive (false);
				inventory = false;
			}
		}	
	}

	void Update () {
		Inventory ();
	}
}
