﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthPotion : Item {

	public float healAmount;

	public override void Use()
	{
		player.GetComponent<PlayerStats> ().playerStats.currentHP += healAmount;
	}
}
