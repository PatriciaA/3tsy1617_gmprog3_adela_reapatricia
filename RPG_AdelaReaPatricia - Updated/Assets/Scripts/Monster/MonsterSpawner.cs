﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterSpawner : MonoBehaviour {

	public GameObject monster;
	public Transform spawnPoint;
	public int maxSpawn;
	public GameObject koko;
	public int curSpawn;
	float spawnRate = 5.0f;

	void SpawnMonster(){
		GameObject mon = Instantiate (monster, spawnPoint.position + new Vector3(Random.Range(this.transform.position.x - 0.1f, this.transform.position.x + 0.1f), 0, Random.Range(this.transform.position.z - 0.1f, this.transform.position.z + 0.1f)), Quaternion.identity) as GameObject;
		mon.transform.SetParent (this.transform);
		curSpawn++;
	}

	void Update(){
		spawnRate -= Time.deltaTime;

		if (spawnRate <= 0 && curSpawn <= maxSpawn) {
			SpawnMonster ();
			spawnRate = 5.0f;
		}
	}
}
