﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Goble_Animation : MonoBehaviour {

	Animator animator;

	void Start()
	{
		animator = GetComponent<Animator> ();
	}

	public void Idle()
	{
		animator.SetTrigger ("Idle");
	}
	public void Walk()
	{
		animator.SetTrigger ("Walk");
	}
	public void Run()
	{
		animator.SetTrigger ("Run");
	}
	public void Attack()
	{ 
		animator.SetTrigger ("Attack");
	}
	public void Dead()
	{
		animator.SetTrigger ("Dead");
	}
	public void Damage()
	{
		animator.SetTrigger ("Damage");
	}
	public void Attack1()
	{
		animator.SetTrigger ("Attack 1");
	}
}
