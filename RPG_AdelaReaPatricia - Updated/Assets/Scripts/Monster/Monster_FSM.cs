﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
public class Monster_FSM : MonoBehaviour {

	public enum State
	{
		IDLE,
		PATROL,
		CHASE,
		ATTACK,
		DYING,
		DEAD
	}
	public State currentState;
	Animator animator;
	public UnityEngine.AI.NavMeshAgent agent;
	public float waitCount;
	float startWaitCount;
	public bool waiting = false;
	Vector3 destination;
	public GameObject target;
	public float dyingTime = 5f;
	public GameObject[] itemDrops;
	void Start()
	{
        
		startWaitCount = waitCount;
		currentState = State.IDLE;
		animator = GetComponent<Animator> ();
		agent = GetComponent<UnityEngine.AI.NavMeshAgent> ();
		agent.speed = 2.0f;
		destination = new Vector3(
			Random.Range(this.transform.position.x - 10, this.transform.position.x + 10),
			0,
			Random.Range(this.transform.position.z - 10, this.transform.position.z + 10));

	}

	void Update()
	{
		switch (currentState) 
		{
		case State.IDLE: UpdateIdleState (); break;
		case State.PATROL: UpdatePatrolState (); break;
		case State.CHASE: UpdateChaseState (); break;
		case State.ATTACK: UpdateAttackState (); break;
		case State.DYING: UpdateDyingState (); break;
		case State.DEAD: UpdateDeadState (); break; 
		}
	}

	void UpdateIdleState(){
		animator.SetTrigger ("Idle");
		FindTarget ();
		waiting = true;
		if (waiting) 
		{
			waitCount -= Time.deltaTime;
			if (waitCount <= 0) 
			{
				waitCount = startWaitCount;
				waiting = false;
				animator.SetTrigger("Walk");
				currentState = State.PATROL;
			}
		}
	}

	void UpdatePatrolState(){
		FindTarget ();
		agent.SetDestination (destination);

		if (agent.remainingDistance < 0.1f) 
		{
			float posX = Random.Range (this.transform.position.x - 10, this.transform.position.x + 10);
			float posZ = Random.Range (this.transform.position.z - 10, this.transform.position.z + 10);
			Vector3 newPos = new Vector3 (posX, 0, posZ);
			destination = newPos;
			currentState = State.IDLE;
		}
	}

	void UpdateChaseState(){
		HealthCheck ();
		animator.SetTrigger ("Run");
		destination = target.transform.position;
		agent.SetDestination (destination);

		if (agent.remainingDistance > 5.0f) {
			target = null;
			currentState = State.IDLE;
		}

		if (agent.remainingDistance < 2.0f){
			currentState = State.ATTACK;
		}

	}

	void UpdateAttackState(){
		//play attack anim
		HealthCheck();
		destination = this.transform.position;
		agent.SetDestination (destination);
		animator.SetTrigger("Attack");
		if (Vector3.Distance(this.transform.position, target.transform.position) > 2.0f) {
			currentState = State.CHASE;
		}
	}

	void UpdateDyingState(){		
		target.GetComponent<Koko_FSM> ().target = null;
        dyingTime -= Time.deltaTime;

		if (dyingTime <= 0) {
            Instantiate(itemDrops[Random.Range(0, itemDrops.Length)], this.transform.position + new Vector3(0,0.5f,0), Quaternion.identity);
			currentState = State.DEAD;
            
		}
	}

	void UpdateDeadState(){
        //play dead anim
        
        target.GetComponent<PlayerStats> ().AddExp (100);
        Destroy(this.gameObject);
    }

	void FindTarget()
	{
		Collider[] col = Physics.OverlapSphere (this.transform.position, 5.0f);
		for (int i = 0; i < col.Length; i++) 
		{
			if (col [i].CompareTag ("Player")) 
			{
				target = col [i].gameObject;
				this.transform.LookAt (destination);
				animator.SetTrigger("Run");
				currentState = State.CHASE;
			}
		}
	}

	void DamageTo(){
		target.GetComponent<PlayerStats> ().playerStats.currentHP -= this.GetComponent<MonsterStats> ().monsterStats.ATK;
	}

	void HealthCheck()
	{
		if (this.GetComponent<MonsterStats> ().monsterStats.currentHP <= 0) {
			animator.SetTrigger ("Dead");
			currentState = State.DYING;
            
        }
	}
}
