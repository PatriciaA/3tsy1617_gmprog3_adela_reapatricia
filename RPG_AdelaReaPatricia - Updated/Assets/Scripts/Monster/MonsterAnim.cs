﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterAnim : MonoBehaviour {

	public Animator monster1, monster2, monster3;

	public void PlayAnimation(float animation){
		monster1.SetFloat ("Monster_Animation", animation);
		monster2.SetFloat ("Monster_Animation", animation);
		monster3.SetFloat ("Monster_Animation", animation);
	}


}
