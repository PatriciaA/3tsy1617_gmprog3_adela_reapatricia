﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TogglePointer : MonoBehaviour {

	public bool targeted;
	public GameObject pointer;

	void Awake(){
		targeted = false;
	}

	void Update(){
		if (targeted) {
			pointer.SetActive (true);
		}
		else {
			pointer.SetActive (false);
		}
	}
}
