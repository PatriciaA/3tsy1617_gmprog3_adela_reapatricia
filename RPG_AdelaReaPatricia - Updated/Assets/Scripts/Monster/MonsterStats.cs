﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[System.Serializable]
public class monStats{

	public float STR;
	public float VIT;
	public float currentHP;


	public float maxHP{
		get { return VIT * 10; }
		set { maxHP = value; }
	}

	public float ATK{
		get { return STR * 10; }
		set { ATK = value; }
	}
}

public class MonsterStats : MonoBehaviour {

	public Stats monsterStats;

	void Start(){
		monsterStats.STR = 2;
		monsterStats.VIT = 5;
		monsterStats.currentXP = 0;
		monsterStats.maxXP = 100;
		monsterStats.currentHP = monsterStats.maxHP;
	}

	void TakeDamage(float damage){
		monsterStats.currentHP -= damage;
	}
}