﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MonsterCombat : MonoBehaviour {

	public GameObject target;
	public float attackrange;
	public float damage;
	bool isAttacking;
	Animator anim;
	// Use this for initialization
	void Start () {
		isAttacking = false;
		anim = GetComponent<Animator> ();
		damage = this.GetComponent<MonsterStats> ().monsterStats.ATK;
	}

	void DamageTo(){
		target.GetComponent<PlayerStats>().playerStats.currentHP -= damage;
		Debug.Log ("Attacking Koko");
	}	
}		

