﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPCAnim : MonoBehaviour {

	public Animator npc1, npc2;

	public void PlayAnimation(float animation){
		npc1.SetFloat ("NPC_Animation", animation);
		npc2.SetFloat ("NPC_Animation", animation);
	}
}
