﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cam_FollowKoko : MonoBehaviour {

	public GameObject koko;

	void Update(){

		this.transform.position = Vector3.Lerp (this.transform.position, koko.transform.position + new Vector3 (12, 11, 10), 0.1f);
	}
}
