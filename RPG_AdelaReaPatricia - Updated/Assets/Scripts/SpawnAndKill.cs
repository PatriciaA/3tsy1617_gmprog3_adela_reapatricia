﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpawnAndKill : MonoBehaviour {

	public GameObject goble;
	public GameObject sla;
	public GameObject pig;

	public GameObject[] reward;

	void KillMonster(GameObject monster){
		Destroy (monster);
		Instantiate (reward [Random.Range (0, reward.Length)], monster.transform.position, Quaternion.identity);
	}

	void Update(){
		if (Input.GetKeyDown (KeyCode.Alpha1)) {
			KillMonster (goble);
		}

		if (Input.GetKeyDown (KeyCode.Alpha2)) {
			KillMonster (sla);
		}

		if (Input.GetKeyDown (KeyCode.Alpha3)) {
			KillMonster (pig);
		}
	}
}
