﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraPanToModel : MonoBehaviour {

	public Transform playerCam, monsterCam, npcCam;

	public GameObject LoadObjects;
	public GameObject target;

	public void MoveCam(){
		target = LoadObjects.GetComponent<ShowButtonsAndModel> ().target;

		switch (target.tag) {
		case "Player":
			this.transform.position = playerCam.position;
			break;
		case "Monster":
			this.transform.position = monsterCam.position;
			break;
		case "NPC":
			this.transform.position = npcCam.position;
			break;
		}
	}
}
