﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShowButtonsAndModel : MonoBehaviour {

	public GameObject koko, kokoButtons;
	public GameObject monster, monsterButtons;
	public GameObject npc, npcButtons;

	public GameObject target = null;

	public void LoadPlayer(){
		npc.SetActive (false);
		npcButtons.SetActive (false);
		monster.SetActive (false);
		monsterButtons.SetActive (false);

		koko.SetActive (true);
		kokoButtons.SetActive (true);
		target = koko;
	}

	public void LoadMonsters(){
		npc.SetActive (false);
		npcButtons.SetActive (false);
		koko.SetActive (false);
		kokoButtons.SetActive (false);

		monster.SetActive (true);
		monsterButtons.SetActive (true);
		target = monster;
	}

	public void LoadNPC(){
		koko.SetActive (false);
		kokoButtons.SetActive (false);

		npc.SetActive (true);
		npcButtons.SetActive (true);
		target = npc;

		monster.SetActive (false);
		monsterButtons.SetActive (false);

	}
}
